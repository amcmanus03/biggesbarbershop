import sqlite3
from datetime import date
import calendar
from datetime import datetime
from flask_mail import Mail, Message
from flask import Flask, render_template, flash, request, url_for, redirect, session, jsonify


app = Flask(__name__)
app.secret_key = 'jhdjbbcksdjbcks'



@app.route("/")
def index():
    return render_template("index.html")
@app.route("/submit", methods=['POST', 'GET'])
def sub():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    day = str(request.form["day"])
    time = str(request.form["options"])
    name = str(request.form["name"])
    datetimes = day + " " + time
    error = ""
    cur.execute("SELECT * FROM booking")
    list = []
    result = cur.fetchall()
    for item in result:
        list.append(item[0])
    a = datetime.strptime(day, '%Y-%m-%d')
    i = 0
    month = ""
    b = day.split("-")
    if b[1] == "01":
        month = "January"
    elif b[1] == "02":
        month = "Febuary"
    elif b[1] == "03":
        month = "March"
    elif b[1] == "04":
        month = "April"
    elif b[1] == "05":
        month = "May"
    elif b[1] == "06":
        month = "June"
    elif b[1] == "07":
        month = "July"
    elif b[1] == "08":
        month = "August"

    if datetimes in list:
        flash("Error, The time you have selected is unavailable")
        return redirect(url_for('index'))
    else:
        query = 'INSERT into booking VALUES (?,?)'
        cur.execute(query, (datetimes, name))
        conn.commit()
        days = calendar.day_name[a.weekday()]
        display = 'Thank you {} for booking an appointment on {}, {} {} at {} we will get in touch with you soon'.format(name,days,month,b[2],time)

        flash(display)
        return redirect(url_for('index'))

@app.route("/admin", methods=['POST', 'GET'])
def admin():
    today = str(datetime.today())
    a = today.split(" ")
    todaysdate =  "%" + a[0] + "%"

    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    #cur.execute("DELETE FROM booking where date !='s'")
    #conn.commit()
    cur.execute("SELECT * FROM booking WHERE date LIKE (?) ORDER BY date ASC",(todaysdate,))

    result = cur.fetchall()
    return render_template("admin.html", result=result)

@app.route("/request", methods=['POST', 'GET'])
def dates():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    day = str(request.form["day"])
    date = "%" + day + "%"
    cur.execute("SELECT * FROM booking WHERE date LIKE (?) ORDER BY date ASC", (date,))
    result = cur.fetchall()
    print(result)
    list = []
    for item in result:
        list.append(item[0].split(" ")[1])
    times = ["9am","10am","11am","12pm","1pm","2pm","3pm","4pm","5pm"]
    return render_template("index.html", times=times,list = list, date=day)



if __name__ == "__main__":
    app.run('0.0.0.0')
